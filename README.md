# SCC - Sistema de Cálculo de Compras

Trata-se de ums sistema de calculo de divisão de custos de compra entre pessoas.

### Funcionamento

Dado uma lista de produtos com quantidade e preços (em centavos) e uma lista de emails (usuários), o sistema calcula a divisão do total da compra entre as pessoas da lista (emails).

Após iniciar o sistema, ele irá solicitar o cadastro dos produtos (nome, quantidade e preço (em centavos)).
Em seguida, o sistema irá solicitar o cadastro das pessoas (email) para quem será dividido o valor total da compra.
Por fim o sistema exibirá o resultado final no seguinte formato (exemplo):
{email1@mail.com = 33, email2@mail.com = 33, email3@mail = 33}

### Regras
O valor total da compra deve ser dividido entre as pessoas.
CADASTROS
É necessário haver ao menos 1 email (pessoa) informado.
Não haverá duplicidade de pessoas (com mesmo email) na lista. O mesmo ocorre com produtos (produtos de mesmo nome não é inserido na lista).

ERROS
O sistema trata erros de negócio (nenhum usuário informado). E erros inesperados.

RESTO
No cálculo, também foi levado em consideração o resto da divisão. Ou seja, se o valor total da compra for 100 centavos para ser dividido por 3 pessoas, o resultado (em numero inteiro) será 33 centavos para cada um. Se percebermos, 33 x 3 = 99. Neste caso resta 1 centavo. 
Para resolver este caso, o valor de resto deve ser dividido igualmente entre as pessoas da lista e somado ao valor individual. Então, neste exemplo, ficará a primeira pessoa com 34 e as outras duas com 33.

### Testes
A aplicação foi desenvolvida com TDD, e por isso há diversos testes unitários (automatizados). Também foram criados testes de componentes (onde se executa teste em toda a aplicação).

Importante ressaltar que no build da aplicação, todos os testes são executados automaticamente.

# Informações Técnicas!
O sistema foi desenvolvido com as seguintes metodologia:
 - TDD
 - Clean Architecture

O sistema foi desenvolvido com as seguintes tecnologias
 - Java (11)
 - Maven
 - Lombok (*** ATENTAR QUE É NECESSÁRIO TER O PLUGUIN NA IDE, SENÃO OCORRE ERROS DE COMPILAÇÃO ***)
 - JUnit 5
 - Mockito
 - git (gitflow)
 - Eclipse (IDE)
 
Para buildar o projeto, é necessário ter o Java e Maven instalado na maquina. Então execute o comando: 'mvn clean install'
```sh
$ mvn clean install
```

Para executar o projeto, execute o seguinte comando e seguir as instruções no console.
```sh
$ java -jar ./target/scc-service-1.0.1-RELEASE.jar
```
