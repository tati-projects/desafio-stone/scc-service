package com.tguisso.scc.domains;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

public class ProductUnitTest {

	@Test
	public void verifyEquals() {
		Product p1 = new Product("A", 0, 0);
		Product p2 = new Product("A", 0, 1);
		
		Set<Product> products = new LinkedHashSet<>();
		products.add(p1);
		products.add(p2);
		
		
		assertTrue(p1.equals(p2));
		assertEquals(1, products.size());
	}
}
