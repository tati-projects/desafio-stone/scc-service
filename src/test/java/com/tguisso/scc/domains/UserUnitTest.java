package com.tguisso.scc.domains;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

public class UserUnitTest {

	@Test
	public void verifyEquals() {
		User u1 = new User("a", 0, 0); 
		User u2 = new User("a", 0, 1);

		Set<User> users = new LinkedHashSet<>();
		users.add(u1);
		users.add(u2);
		
		assertTrue(u1.equals(u2));
		assertEquals(1, users.size());
	}
}
