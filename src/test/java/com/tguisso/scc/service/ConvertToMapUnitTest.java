package com.tguisso.scc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.tguisso.scc.domains.User;

public class ConvertToMapUnitTest {
	
	private ConvertToMapService convertToMapService = new ConvertToMapService();
	
	@Test
	public void convertWithNoRest() {
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("fulano@gmail", 20, 0));
		users.add(new User("ciclano@gmail", 20, 0));
		users.add(new User("beltrano@gmail", 20, 0));
		
		Map<String, Integer> result = convertToMapService.convert(users);
		
		assertNotNull(result);
		assertEquals(20, result.get("fulano@gmail"));
		assertEquals(20, result.get("ciclano@gmail"));
		assertEquals(20, result.get("beltrano@gmail"));
	}
	
	@Test
	public void convertWithRest() {
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("fulano@gmail", 158, 0));
		users.add(new User("ciclano@gmail", 158, 0));
		users.add(new User("beltrano@gmail", 158, 2));
		
		Map<String, Integer> result = convertToMapService.convert(users);
		
		assertNotNull(result);
		assertEquals(158, result.get("fulano@gmail"));
		assertEquals(158, result.get("ciclano@gmail"));
		assertEquals(160, result.get("beltrano@gmail"));
	}
	
	@Test
	public void convertWithNoUsers() {
		Set<User> users = new LinkedHashSet<>();
				
		Map<String, Integer> result = convertToMapService.convert(users);
		
		assertNotNull(result);
		assertEquals(0, result.size());
		
	}
	
}
