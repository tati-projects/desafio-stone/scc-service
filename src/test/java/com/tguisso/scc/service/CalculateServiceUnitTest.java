package com.tguisso.scc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.scc.domains.Product;
import com.tguisso.scc.domains.User;
import com.tguisso.scc.exception.OneUserIsRequiredException;

@ExtendWith(MockitoExtension.class)
	public class CalculateServiceUnitTest {
	
	@InjectMocks
	private CalculateService calculateService;
	
	@Mock
	private CalculateTotalCostService calculateTotalCostService;
	
	@Mock
	private CalculateIndividualCostService calculateIndividualCostService;
	
	@Mock
	private CalculateRestService calculateRestService;
	
	@Mock
	private DistributeCostService distributeCostService;
	
	@Mock
	private ConvertToMapService convertToMapService; 
	
	@Test
	public void calculate() {
		Set<Product> products = new LinkedHashSet<>();
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("fulano@gmail.com", 0, 0));
		
		int totalCost = 500;
		doReturn(totalCost).when(calculateTotalCostService).calculate(products);
		
		int individualCost = 500;
		doReturn(individualCost).when(calculateIndividualCostService).calculate(totalCost, users.size());
		
		int rest = 0;
		doReturn(rest).when(calculateRestService).calculate(totalCost, individualCost, users.size());
		
		Map<String, Integer> result = new LinkedHashMap<>();
		doReturn(result).when(convertToMapService).convert(users);
		
		Map<String, Integer> resultReturned = calculateService.calculate(products, users);
		
		verify(calculateTotalCostService).calculate(products);
		verify(calculateIndividualCostService).calculate(totalCost, users.size());
		verify(calculateRestService).calculate(totalCost, individualCost, users.size());
		verify(distributeCostService).distribute(users, individualCost, rest);
		verify(convertToMapService).convert(users);
		
		assertEquals(result, resultReturned);
		assertNotNull(users);
		assertNotNull(products);
	}
	
	@Test
	public void calculateWithNoUserExpectedException() {
		Set<Product> products = new LinkedHashSet<>();
		Set<User> users = new LinkedHashSet<>();
		
		assertThrows(OneUserIsRequiredException.class, () -> {
			calculateService.calculate(products, users);
		});
	}
	
}
