package com.tguisso.scc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.tguisso.scc.domains.User;

public class DistributeCostServiceUnitTest {
	
private DistributeCostService distributeCostService = new DistributeCostService();
	
	@Test
	public void distributeCostWithNoRest() {
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("fulano@gmail", 0, 0));
		users.add(new User("ciclano@gmail", 0, 0));

		int individualCost = 50;
		int rest = 0;
		
		distributeCostService.distribute(users, individualCost, rest);
		
		for (User user : users) {
			assertEquals(individualCost, user.getIndividualCost());
			assertEquals(0, user.getRest());
		}
	}
	
	@Test
	public void distributeCostWithRestA() {
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("fulano@gmail", 0, 0));
		users.add(new User("ciclano@gmail", 0, 0));

		int individualCost = 50;
		int rest = 1;
		
		distributeCostService.distribute(users, individualCost, rest);
		
		for (User user : users) {
			assertEquals(individualCost, user.getIndividualCost());
		}
		int totalRest = 0;
		for (User user : users) {
			totalRest += user.getRest();
			if(user.getEmail().equals("fulano@gmail")) {
				assertEquals(1, user.getRest());
			}
			if(user.getEmail().equals("ciclano@gmail")) {
				assertEquals(0, user.getRest());
			}
		}
		assertEquals(rest, totalRest);
	}
	
	@Test
	public void distributeCostWithRestB() {
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("fulano@gmail", 0, 0));
		users.add(new User("ciclano@gmail", 0, 0));

		int individualCost = 50;
		int rest = 3;
		
		distributeCostService.distribute(users, individualCost, rest);
		
		for (User user : users) {
			assertEquals(individualCost, user.getIndividualCost());
		}
		int totalRest = 0;
		for (User user : users) {
			totalRest += user.getRest();
			if(user.getEmail().equals("fulano@gmail")) {
				assertEquals(2, user.getRest());
			}
			if(user.getEmail().equals("ciclano@gmail")) {
				assertEquals(1, user.getRest());
			}
		}
		assertEquals(rest, totalRest);
	}
	
	@Test
	public void distributeCostWithRestC() {
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("fulano@gmail", 0, 0));
		users.add(new User("ciclano@gmail", 0, 0));
		users.add(new User("beltrano@gmail", 0, 0));

		int individualCost = 50;
		int rest = 3;
		
		distributeCostService.distribute(users, individualCost, rest);
		
		int totalRest = 0;
		for (User user : users) {
			totalRest += user.getRest();
			assertEquals(individualCost, user.getIndividualCost());
			assertEquals(1, user.getRest());
		}
		assertEquals(rest, totalRest);
	}
	
	@Test
	public void distributeCostWithRestD() {
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("fulano@gmail", 0, 0));
		users.add(new User("ciclano@gmail", 0, 0));
		users.add(new User("beltrano@gmail", 0, 0));
		users.add(new User("marciano@gmail", 0, 0));

		int individualCost = 80;
		int rest = 25;
		
		distributeCostService.distribute(users, individualCost, rest);
		
		for (User user : users) {
			assertEquals(individualCost, user.getIndividualCost());
		}
		int totalRest = 0;
		for (User user : users) {
			totalRest += user.getRest();
			if(user.getEmail().equals("fulano@gmail")) {
				assertEquals(7, user.getRest());
			}
			if(user.getEmail().equals("ciclano@gmail")) {
				assertEquals(6, user.getRest());
			}
			if(user.getEmail().equals("beltrano@gmail")) {
				assertEquals(6, user.getRest());
			}
			if(user.getEmail().equals("marciano@gmail")) {
				assertEquals(6, user.getRest());
			}
		}
		assertEquals(rest, totalRest);
	}
	
	@Test
	public void distributeWithNoUser() {
		Set<User> users = new LinkedHashSet<>();

		int individualCost = 50;
		int rest = 0;
		
		distributeCostService.distribute(users, individualCost, rest);
		
		assertEquals(0, users.size());
	}

}
