package com.tguisso.scc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CalculateRestServiceUnitTest {
	
	private CalculateRestService calculateRestService = new CalculateRestService();
	
	@Test
	public void calculateWithNoRest() {
		int totalCost = 100;
		int individualcost = 50;
		int usersAmount = 2;
		
		int rest = calculateRestService.calculate(totalCost, individualcost, usersAmount);
		
		assertEquals(0, rest);
	}
	
	@Test
	public void calculateWithRest() {
		int totalCost = 100;
		int individualcost = 33;
		int usersAmount = 3;
		
		int rest = calculateRestService.calculate(totalCost, individualcost, usersAmount);
		
		assertEquals(1, rest);
	}

}
