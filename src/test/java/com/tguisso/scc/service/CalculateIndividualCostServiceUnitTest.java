package com.tguisso.scc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.tguisso.scc.exception.OneUserIsRequiredException;

public class CalculateIndividualCostServiceUnitTest {
	
	private CalculateIndividualCostService calculateIndividualCostService = new CalculateIndividualCostService();
	
	@Test
	public void calculateWithTwoUsers() {
		int totalCost = 100; 
		int usersAmount = 2;
		
		int individualValue = calculateIndividualCostService.calculate(totalCost, usersAmount);
		
		assertEquals(50, individualValue);
	}
	
	@Test
	public void calculateWithMoreUsers() {
		int totalCost = 100; 
		int usersAmount = 3;
		
		int individualValue = calculateIndividualCostService.calculate(totalCost, usersAmount);
		
		assertEquals(33, individualValue);
	}
	
	@Test
	public void calculateNoCostNoUser() {
		int totalCost = 0; 
		int usersAmount = 0;
		
		assertThrows(OneUserIsRequiredException.class, () -> {
			calculateIndividualCostService.calculate(totalCost, usersAmount);
		});
	}

}
