package com.tguisso.scc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.tguisso.scc.domains.Product;

public class CalculateTotalCostServiceUnitTest {
	
	private CalculateTotalCostService calculateTotalCostService = new CalculateTotalCostService();
	
	@Test
	public void calculateWithOneProduct() {
		Set<Product> products = new LinkedHashSet<>();
		products.add(new Product("refrigerante", 1, 100));
		
		int totalCost = calculateTotalCostService.calculate(products);
		
		assertEquals(100, totalCost);
	}
	
	@Test
	public void calculateWithTwoProducts() {
		Set<Product> products = new LinkedHashSet<>();
		products.add(new Product("refrigerante", 1549, 500));
		products.add(new Product("chips", 254, 297));
		
		int totalCost = calculateTotalCostService.calculate(products);
		
		assertEquals(849938, totalCost);
	}
	
	@Test
	public void calculateWithNoProducts() {
		Set<Product> products = new LinkedHashSet<>();
		
		int totalCost = calculateTotalCostService.calculate(products);
		
		assertEquals(0, totalCost);
	}


}
