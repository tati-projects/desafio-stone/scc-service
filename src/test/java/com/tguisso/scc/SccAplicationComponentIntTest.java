package com.tguisso.scc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.tguisso.scc.domains.Product;
import com.tguisso.scc.domains.User;
import com.tguisso.scc.exception.OneUserIsRequiredException;
import com.tguisso.scc.service.CalculateService;

public class SccAplicationComponentIntTest {
	
	private CalculateService calculateService = new CalculateService();
	
	@Test
	public void successWithNoRest() {
		Set<Product> products = new LinkedHashSet<>();
		products.add(new Product("refrigerante", 5, 600));
		
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("any@gmail", 0, 0));
		users.add(new User("fulano@gmail", 0, 0));
		
		Map<String, Integer> result = calculateService.calculate(products, users);
		
		assertEquals(1500, result.get("any@gmail"));
		assertEquals(1500, result.get("fulano@gmail"));
		
	}
	
	@Test
	public void successWithRest() {
		Set<Product> products = new LinkedHashSet<>();
		products.add(new Product("refrigerante", 1, 50));
		products.add(new Product("suco", 1, 50));
		
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("any@gmail", 0, 0));
		users.add(new User("fulano@gmail", 0, 0));
		users.add(new User("ciclano@gmail", 0, 0));
		
		Map<String, Integer> result = calculateService.calculate(products, users);
		
		assertEquals(34, result.get("any@gmail"));
		assertEquals(33, result.get("fulano@gmail"));
		assertEquals(33, result.get("ciclano@gmail"));
	}
	
	@Test
	public void noUserException() {
		Set<Product> products = new LinkedHashSet<>();
		products.add(new Product("refrigerante", 1, 50));
		products.add(new Product("suco", 1, 50));
		
		Set<User> users = new LinkedHashSet<>();
		
		assertThrows(OneUserIsRequiredException.class, () -> {
			calculateService.calculate(products, users);
		});
	}
	
	@Test
	public void noProduct() {
		Set<Product> products = new LinkedHashSet<>();
			
		Set<User> users = new LinkedHashSet<>();
		users.add(new User("any@gmail", 0, 0));
		
		Map<String, Integer> result = calculateService.calculate(products, users);
		
		assertEquals(0, result.get("any@gmail"));
	}

}
