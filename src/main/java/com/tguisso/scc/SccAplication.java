package com.tguisso.scc;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.tguisso.scc.domains.Product;
import com.tguisso.scc.domains.User;
import com.tguisso.scc.exception.OneUserIsRequiredException;
import com.tguisso.scc.service.CalculateService;

public class SccAplication {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		try {
			CalculateService calculateService = new CalculateService();
			Set<Product> products = new LinkedHashSet<>();
			Set<User> users = new LinkedHashSet<>();

			getProducts(sc, products);
			getEmails(sc, users);
			showResult(calculateService, products, users);

		} catch (OneUserIsRequiredException e) {
			System.err.println("Ops! " + e.getMessage());
			e.printStackTrace();
			
		} catch (Exception e) {
			System.err.println("Ops! Ocorreu um erro. Tente novamente.");
			e.printStackTrace();
			
		} finally {
			System.out.println();
			System.out.println("Fim do programa!");
			sc.close();
		}
	}

	private static void showResult(CalculateService calculateService, Set<Product> products, Set<User> users) {
		Map<String, Integer> result = calculateService.calculate(products, users);
		System.out.println("Resultado: ");
		System.out.println(result);
	}

	private static void getEmails(Scanner sc, Set<User> users) {
		System.out.print("Para quantas pessoas será divido o valor da compra? ");
		int usersAmount = sc.nextInt();
		sc.nextLine();
		System.out.println();

		while(usersAmount == 0) {
			System.out.println("Deve ser informado ao menos 1 pessoa. Tente novamente.");
			usersAmount = sc.nextInt();
			sc.nextLine();
		}
		
		if(usersAmount > 0) {
			System.out.println("ATENÇÃO: Pessoas com o mesmo email não serão inseridos na lista.");
			System.out.println();
			System.out.println("Informe o e-mail de cada um:");
			
			for(int i = 0; i < usersAmount; i++) {
				String email = sc.nextLine();
				users.add(new User(email, 0, 0));
			}
			
		}
	}

	private static void getProducts(Scanner sc, Set<Product> products) {
		System.out.println("Por favor informe abaixo o produto com a quantidade e preço unitário.");
		System.out.println("ATENÇÃO: Produtos com o mesmo nome não serão computados na lista.");
		System.out.println();
		char response = 's';
		while (response == 's' || response == 'S') {
			System.out.print("Nome do produto: ");
			String name = sc.nextLine();
			System.out.print("Quantidade: ");
			Integer quantity = sc.nextInt();
			System.out.print("Preço (em centavos): ");
			Integer price = sc.nextInt();
			sc.nextLine();
			products.add(new Product(name, quantity, price));
			System.out.print("Deseja incluir outro produto (s/n)? ");
			response = sc.next().charAt(0);
			sc.nextLine();
			System.out.println();
		}
	}

}
