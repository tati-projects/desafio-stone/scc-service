package com.tguisso.scc.service;

import java.util.Map;
import java.util.Set;

import com.tguisso.scc.domains.Product;
import com.tguisso.scc.domains.User;
import com.tguisso.scc.exception.OneUserIsRequiredException;

public class CalculateService {
	
	private CalculateTotalCostService calculateTotalCostService = new CalculateTotalCostService();
	private CalculateIndividualCostService calculateIndividualCostService = new CalculateIndividualCostService();
	private CalculateRestService calculateRestService = new CalculateRestService();
	private DistributeCostService distributeCostService = new DistributeCostService();
	private ConvertToMapService convertToMapService = new ConvertToMapService();

	public Map<String, Integer> calculate(Set<Product> products, Set<User> users) {
		
		validateData(users);
				
		int totalCost = calculateTotalCostService.calculate(products);
		
		int individualCost = calculateIndividualCostService.calculate(totalCost, users.size());
		
		int rest = calculateRestService.calculate(totalCost, individualCost, users.size());
		
		distributeCostService.distribute(users, individualCost, rest);
		
		return convertToMapService.convert(users);
	}

	private void validateData(Set<User> users) {
		if(users.isEmpty()) {
			throw new OneUserIsRequiredException();
		}
	}
	
}
