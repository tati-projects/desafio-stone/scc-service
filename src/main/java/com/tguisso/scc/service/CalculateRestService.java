package com.tguisso.scc.service;

public class CalculateRestService {

	public int calculate(final int totalCost, final int individualCost, final int usersAmount) {
		int totalIndividualCost = individualCost * usersAmount;
		return totalCost - totalIndividualCost;
	}
	
}
