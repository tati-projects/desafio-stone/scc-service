package com.tguisso.scc.service;

import java.util.Set;

import com.tguisso.scc.domains.Product;

public class CalculateTotalCostService {

	public int calculate(Set<Product> products) {
		int totalCost = 0;
		for(Product product : products) {
			totalCost += product.getTotalCost();
		}
		return totalCost;
	}

}
