package com.tguisso.scc.service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.tguisso.scc.domains.User;

public class ConvertToMapService {
	
	public Map<String, Integer> convert(Set<User> users){
		Map<String, Integer> result = new LinkedHashMap<>();
		for(User user : users) {
			result.put(user.getEmail(), user.getTotalCost());
		}
		return result;
	}
}
