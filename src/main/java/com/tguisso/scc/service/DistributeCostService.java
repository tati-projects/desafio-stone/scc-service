package com.tguisso.scc.service;

import java.util.Set;

import com.tguisso.scc.domains.User;

public class DistributeCostService {

	public void distribute(Set<User> users, int individualCost, int rest) {
		setIndividualCost(users, individualCost);
		setIndividualRest(users, rest);
	}

	private void setIndividualRest(Set<User> users, int rest) {
		int restAux = rest;
		while(restAux > 0) {
			for(User user : users) {
				user.setRest(user.getRest() + 1);
				restAux--;
				if(restAux <= 0) {
					break;
				}
			}
		}
	}

	private void setIndividualCost(Set<User> users, int individualCost) {
		for(User user : users) {
			user.setIndividualCost(individualCost);
		}
	}
}
