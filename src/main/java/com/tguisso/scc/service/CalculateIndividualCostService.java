package com.tguisso.scc.service;

import com.tguisso.scc.exception.OneUserIsRequiredException;

public class CalculateIndividualCostService {

	public int calculate(int totalCost, int usersAmount) {
		if(usersAmount == 0) {
			throw new OneUserIsRequiredException();
		}
		return totalCost / usersAmount; 
	}
}
