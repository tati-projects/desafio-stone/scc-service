package com.tguisso.scc.domains;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
@EqualsAndHashCode(exclude = {"individualCost", "rest"})
public class User {
	private String email;
	private Integer individualCost = 0;
	private Integer rest = 0;
	
	public Integer getTotalCost() {
		return individualCost + rest;
	}

	public void setIndividualCost(Integer individualCost) {
		this.individualCost = individualCost;
	}

	public void setRest(Integer rest) {
		this.rest = rest;
	}
}
