package com.tguisso.scc.domains;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"quantity", "price"})
public class Product {
	private String name;
	private Integer quantity;
	private Integer price;
	
	public int getTotalCost() {
		return price * quantity;
	}
}
