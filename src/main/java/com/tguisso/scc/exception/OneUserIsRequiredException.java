package com.tguisso.scc.exception;

import lombok.Getter;

@Getter
public class OneUserIsRequiredException extends RuntimeException{
	private static final long serialVersionUID = -773270382062770414L;
	
	private String code = "scc.oneUserRequired";
	private String message = "É necessário haver ao menos 1 usuário.";
}
